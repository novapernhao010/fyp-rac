<?php 
  if (!isset($_SESSION['User_ID'])) {
	  $_SESSION['msg'] = "You must log in first";
	  echo 'You dont have permission to access this page. <a href="index.php" data-target="#exampleModal">Back To Main</a>';
	  die();
	  header('location: index.php');
  }else{
		$userData = $_SESSION["User_ID"];
  }

  if (isset($_GET['logout'])) {
  	session_destroy();
	unset($_SESSION['User_ID']);
	header("refresh:0.3;url=index.php");
  } 

?>
	<!-- top-header -->
	<div class="agile-main-top">
		<div class="container-fluid">
			<div class="row main-top-w3l py-2">
				<div class="col-lg-4 header-most-top">
					<p class="text-white text-lg-left text-center">Offer Zone Top Deals & Discounts
						<i class="fas fa-shopping-cart ml-1"></i>
					</p>
				</div>
				<div class="col-lg-8 header-right mt-lg-0 mt-2">
					<!-- header lists -->
					<ul>
						<li class="text-center border-right text-white">
							<a class="play-icon popup-with-zoom-anim text-white" href="#small-dialog1">
								<i class="fas fa-map-marker mr-2"></i>Select Location</a>
						</li>
						<li class="text-center border-right text-white">
							<a href="#" data-toggle="modal" data-target="#exampleModal" class="text-white">
								<i class="fas fa-truck mr-2"></i>Track Order</a>
						</li>
						<li class="text-center border-right text-white">
							<i class="fas fa-phone mr-2"></i> 06 - 953 9137
						</li>
						<li class="text-center border-right text-white">
								<i class="fas mr-2"></i>Welcome<a href="profile.php"> <strong style="color:yellow;"><?php echo $userData['User_name']; ?> </strong> </a>
						</li>
						<li class="text-center text-white">
								<i class="fas fa-sign-out-alt mr-2"></i><a href="index2.php?logout='1'" id="logout" style="color: red;font-style:italic">Logout</a> 
						</li>			
					</ul>
					<!-- //header lists -->
				</div>
			</div>
		</div>
	</div>
    <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h3>
          <?php 
          	echo $_SESSION['success']; 
          	unset($_SESSION['success']);
          ?>
      	</h3>
      </div>
  	<?php endif ?>

	<!-- Button trigger modal(select-location) -->
	<div id="small-dialog1" class="mfp-hide">
		<div class="select-city">
			<h3>
				<i class="fas fa-map-marker"></i> Please Select Your Location</h3>
			<select class="list_of_cities">
				<optgroup label="Popular Cities">
					<option selected style="display:none;color:#eee;">Select City</option>
					<option>Kuala Lumpur</option>
					<option>Melaka</option>
					<option>Muar</option>
				</optgroup>
				<optgroup label="Negeri Sembilan">
					<option>Seremban</option>
				</optgroup>
				<optgroup label="Malacca">
					<option>Malacca City</option>
				</optgroup>
				<optgroup label="Johor">
					<option>Johor Bahru</option>
					<option>Tangkak</option>
					<opiton>Bukit Gambir</opiton>
					<option>Muar</option>
					<option>Bukit Pasir</option>
				</optgroup>
				<optgroup label="Kedah">
					<option>Alor Setar</option>
				</optgroup>
				<optgroup label="Kelantan">
					<option>Kota Bahru</option>
				</optgroup>
				<optgroup label="Pahang">
					<option>Kuantan</option>
				</optgroup>
				<optgroup label="Perlis">
					<option>Kangar</option>
				</optgroup>
				<optgroup label="Perak">
					<option>Ipoh</option>	
				</optgroup>
				<optgroup label="Selangor">
					<option>Shah Alam</option>
				</optgroup>
				<optgroup label="Penang">
					<option>George Town</option>
				</optgroup>
				<optgroup label="Terengganu">
					<option>Kuala Terengganu</option>
				</optgroup>
				<optgroup></optgroup>
				<optgroup label="East Malaysia ">
					<optgroup label="Sarawak">
						<option>Kuching</option>
						<option>Sibu</option>
					</optgroup>
					<optgroup label="Sabah">
						<option>Sandakan</option>
					</optgroup>
				</optgroup>
			</select>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //shop locator (popup) -->

	<!-- //top-header -->

	<!-- header-bottom-->
	<div class="header-bot">
		<div class="container">
			<div class="row header-bot_inner_wthreeinfo_header_mid">
				<!-- logo -->
				<div class="col-md-3 logo_agile">
					<h1 class="text-center">
						<a href="index2.php" class="font-weight-bold font-italic">
							<img src="images/logo3.jpg" alt=" " class="img-fluid" style="padding-top:7px">&nbsp&nbsp&nbspAccessories
						</a>
					</h1>
				</div>
				<!-- //logo -->
				<!-- header-bot -->
				<div class="col-md-9 header mt-4 mb-md-0 mb-4">
					<div class="row">
						<!-- search -->
						<div class="col-10 agileits_search">
							<form class="form-inline" action="#" method="post">
								<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" required>
								<button class="btn my-2 my-sm-0" type="submit">Search</button>
							</form>
						</div>
						<!-- //search -->
						<!-- cart details -->
						<div class="col-2 top_nav_right text-center mt-sm-0 mt-2">
							<div class="wthreecartaits wthreecartaits2 cart cart box_1">
								<form action="#" method="post" class="last">
									<input type="hidden" name="cmd" value="_cart">
									<input type="hidden" name="display" value="1">
									<button class="btn w3view-cart" type="submit" name="submit" value="">
										<i class="fas fa-cart-arrow-down"></i>
									</button>
								</form>
							</div>
						</div>
						<!-- //cart details -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- shop locator (popup) -->
	<!-- //header-bottom -->
	<!-- navigation -->
	<div class="navbar-inner">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="agileits-navi_search">
					<form action="#" method="post">
						<select id="agileinfo-nav_search" name="agileinfo_search" class="border" required="">
							<option value="">All Categories</option>
							<?php
								$result=mysqli_query($db,"SELECT * from category");
								while($row=mysqli_fetch_assoc($result))
								{
							?>
							<option value="<?php echo $row['Category_ID'] ?>"><?php echo $row['Category_name'] ?></option>
							<?php } ?>
						</select>
					</form>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto text-center mr-xl-5">
						<li class="nav-item active mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link" href="index2.php">Home
								<span class="sr-only">(current)</span>
							</a>
						</li>
						<li class="nav-item dropdown mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Categories
							</a>
							<div class="dropdown-menu">
								<div class="agile_inner_drop_nav_info p-4">
									<!-- <h5 class="mb-3">Mobiles, Computers</h5> -->
									<div class="row">
										<div class="col-sm-6 multi-gd-img">
											<ul class="multi-column-dropdown">
												<?php 
													$result2=mysqli_query($db,"SELECT * from category");
													$count=0;
													while($row2=mysqli_fetch_assoc($result2))
													{
														if($count==6)
														{
															?>
																</ul>
															</div>
															<div class="col-sm-6 multi-gd-img">
																<ul class="multi-column-dropdown">
																	<li>
																		<a href="product2.php?cate_id=<?php echo $row2['Category_ID'] ?>" style="color:blue;text-decoration:underline;"><?php echo $row2['Category_name'] ?></a>
																		<ul>
																			<?php
																				$result3=mysqli_query($db,"SELECT * from subcategory where cate_ID='".$row2['Category_ID']."'");
																				while($row3=mysqli_fetch_assoc($result3))
																				{
																			?>
																			<li>
																				<a href="product2.php?subcate_id=<?php echo $row3['Subcategory_ID'] ?>"><?php echo $row3['Subcategory_name'] ?></a>
																			</li>
																			<?php } ?>
																		</ul>
																	</li>
															<?php
															continue;
														}
												?>
												<li>
													<a href="product2.php?cate_id=<?php echo $row2['Category_ID'] ?>" style="color:blue;text-decoration:underline;"><?php echo $row2['Category_name'] ?></a>
													<ul>
														<?php
															$result3=mysqli_query($db,"SELECT * from subcategory where cate_ID='".$row2['Category_ID']."'");
															while($row3=mysqli_fetch_assoc($result3))
															{
														?>
														<li>
															<a href="product2.php?subcate_id=<?php echo $row3['Subcategory_ID'] ?>"><?php echo $row3['Subcategory_name'] ?></a>
														</li>
														<?php } ?>
													</ul>
												</li>
												<?php 
													$count++;
													} 
												?>
											</ul>
										</div>
									
									</div>
								</div>
							</div>
						</li>
						
						<li class="nav-item mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link" href="about2.php">About Us</a>
						</li>
						<!-- <li class="nav-item mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link" href="product.php">New Arrivals</a>
						</li> -->
						<li class="nav-item dropdown mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Pages
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="profile.php">Profile Page</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="checkout.php">Checkout Page</a>
								<a class="dropdown-item" href="payment.php">Payment Page</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="faqs2.php">Q&A</a>
								<a class="dropdown-item" href="help2.php">Help Page</a>
								<a class="dropdown-item" href="privacy2.php">Privacy & Policy</a>
								<a class="dropdown-item" href="terms2.php">Terms & Conditions</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="contact2.php">Contact Us</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	<script>
		
		$(function(){
    	$('a#logout').click(function(){
        if(confirm('Are you sure to logout ?')) {
			
            return true;
        }

        return false;
    });
});
		</script>