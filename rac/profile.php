<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">

<?php include("html_head.php"); 
?>
<style>
#box1
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
}
#box2{
	height:150px;
	font-size:12pt;
	border: 2px double grey;
  	border-radius: 4px;
	color:black;
}
</style>

<body>
	<?php include("header2.php"); ?>
	<?php
		if(isset($_SESSION["User_ID"])){
			$userData = $_SESSION["User_ID"];

			$username = $userData["User_name"];
			$uemail = $userData["User_email"];
			$phone = $userData["User_phone"];
			$address = $userData["User_address"];
			$state = $userData["User_state"];
			$postcode = $userData["User_postcode"];
			$gender = $userData["User_gender"];
			$password = $userData["User_password"];
		}
	?>
	<!-- page -->

	<div class="services-breadcrumb">
		<div class="agile_inner_breadcrumb">
			<div class="container">
				<ul class="w3_short">
					<li>
						<a href="index.php">Home</a>
						<i>|</i>
					</li>
					<li><span style="font-style:italic;">Profile</span></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //page -->

	<!-- contact -->
	<div class="contact py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>P</span><span style="font-style:italic;">rofile</span>
			</h3>
			<!-- form -->

			<form action="#" method="post">
            <fieldset>
				<div class="contact-grids1 w3agile-6">
					<div class="row">
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Name</label>
							<input type="text" class="form-control" id="box1" name="Name"  value="<?php echo $username; ?>" disabled>
						</div>
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">E-mail</label>
							<input type="text" class="form-control"  id="box1" name="Email" value="<?php echo $userData['User_email']; ?>"  disabled>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Phone</label>
						<input type="text" class="form-control"  id="box1" name="Phone" value="<?php echo $userData['User_phone']; ?>"  disabled>
					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Gender</label>
						<input type="text" class="form-control"  id="box1" name="Gender" style="text-transform:capitalize;" value="<?php echo $userData['User_gender']; ?>"  disabled>
					</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Postcode</label>
						<input type="text" class="form-control"  id="box1" name="Postcode" value="<?php echo $userData['User_postcode']; ?>"  disabled>
					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">State</label>
						<input type="text" class="form-control"  id="box1" name="State" style="text-transform:capitalize;" value="<?php echo $userData['User_state']; ?>"  disabled>
					</div>
					</div>
					<div class="contact-me animated wow slideInUp form-group">
						<label class="col-form-label">Address</label>
						<input type="text" style="font-weight:bold;" name="Address" id="box2" class="form-control" value="<?php echo $userData['User_address']; ?>"  disabled> 
					</div>
					<div class="contact-form">
						<input type="submit" value="Edit Profile" formaction="edit_profile.php">
					</div>
				</div>
                </fieldset>
			</form>
			<!-- //form -->
		</div>
	</div>

	<?php include("footer2.php");
	?>

	<!-- js-files -->
	<!-- jquery -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- //jquery -->

	<!-- nav smooth scroll -->
	<script>
		$(document).ready(function () {
			$(".dropdown").hover(
				function () {
					$('.dropdown-menu', this).stop(true, true).slideDown("fast");
					$(this).toggleClass('open');
				},
				function () {
					$('.dropdown-menu', this).stop(true, true).slideUp("fast");
					$(this).toggleClass('open');
				}
			);
		});
	</script>
	<!-- //nav smooth scroll -->

	<!-- popup modal (for location)-->
	<script src="js/jquery.magnific-popup.js"></script>
	<script>
		$(document).ready(function () {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});

		});
	</script>
	<!-- //popup modal (for location)-->

	<!-- cart-js -->
	<script src="js/minicart.js"></script>
	<script>
		paypals.minicarts.render(); //use only unique class names other than paypals.minicarts.Also Replace same class name in css and minicart.min.js

		paypals.minicarts.cart.on('checkout', function (evt) {
			var items = this.items(),
				len = items.length,
				total = 0,
				i;

			// Count the number of each item in the cart
			for (i = 0; i < len; i++) {
				total += items[i].get('quantity');
			}

			if (total < 3) {
				alert('The minimum order quantity is 3. Please add more to your shopping cart before checking out');
				evt.preventDefault();
			}
		});
	</script>
	<!-- //cart-js -->

	<!-- password-script -->
	<script>
		window.onload = function () {
			document.getElementById("password1").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}

		function validatePassword() {
			var pass2 = document.getElementById("password2").value;
			var pass1 = document.getElementById("password1").value;
			if (pass1 != pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');
			//empty string means no validation error
		}
	</script>
	<!-- //password-script -->

	<!-- smoothscroll -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- //smoothscroll -->

	<!-- start-smooth-scrolling -->
	<script src="js/move-top.js"></script>
	<script src="js/easing.js"></script>
	<script>
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script>
		$(document).ready(function () {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->

	<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<!-- //js-files -->

</body>

</html>