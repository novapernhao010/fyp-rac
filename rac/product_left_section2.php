<?php 
//include("config.php"); 
?>
    <!-- product left -->
    <div class="agileinfo-ads-display col-lg-9">
        <div class="wrapper">
            <?php 
                $result=mysqli_query($db,"SELECT * from product order by Prod_ID DESC");
                $image_dir="product_img/";
                $count=0;
            ?>
            <!-- first section -->
            <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
                <!-- <h3 class="heading-tittle text-center font-italic">Tv & Audio</h3> -->
                <div class="row">
                <?php 
                    while($row=mysqli_fetch_assoc($result))
                    {
                        $count++;
                ?>
                    <div class="col-md-4 product-men mt-5">
                        <div class="men-pro-item simpleCart_shelfItem">
                            <div class="men-thumb-item text-center">
                                <img src="<?php echo $image_dir.$row['Prod_img1']; ?>" alt="" style="max-width: 100%;max-height: 100%;display:block;width: 200px;">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="single2.php?id=<?php echo $row['Prod_ID']; ?>" class="link-product-add-cart">Quick View</a>
                                    </div>
                                </div>
                            </div>
                            <span class="product-new-top">New</span>
                            <div class="item-info-product text-center border-top mt-4">
                                <h4 class="pt-1">
                                    <a href="single2.php?id=<?php echo $row['Prod_ID']; ?>"><?php echo $row['Prod_name']; ?></a>
                                </h4>
                                <div class="info-product-price my-2">
                                    <span class="item_price">RM<?php echo $row['Prod_price']; ?></span>
                                    <!-- <del>$300.00</del> -->
                                </div>
                                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                    <form action="#" method="post">
                                        <fieldset>
                                            <input type="hidden" name="cmd" value="_cart" />
                                            <input type="hidden" name="add" value="1" />
                                            <input type="hidden" name="business" value=" " />
                                            <input type="hidden" name="item_name" value="<?php echo $row['Prod_name']; ?>" />
                                            <input type="hidden" name="amount" value="<?php echo $row['Prod_price']; ?>" />
                                            <!-- <input type="hidden" name="discount_amount" value="1.00" /> -->
                                            <input type="hidden" name="currency_code" value="RM" />
                                            <input type="hidden" name="return" value=" " />
                                            <input type="hidden" name="cancel_return" value=" " />
                                            <input type="submit" name="submit" value="Add to cart"  id="addbtn" class="button btn" />
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                            if($count==6)
                            {
                                ?>
                                    <!-- closing section to load ads/banner -->
                                    </div>
                                </div>
                                <!-- //closing section to load ads/banner -->
                                <!-- ads section -->
                                <div class="product-sec1 product-sec2 px-sm-5 px-3">
                                    <div class="row">
                                        <h3 class="col-md-4 effect-bg">Summer Carnival</h3>
                                        <p class="w3l-nut-middle">Get Extra 10% Off</p>
                                        <div class="col-md-8 bg-right-nut">
                                            <img src="images/image1.png" alt="" style="max-width: 100%;max-height: 100%;display:block;width:500px;">
                                        </div>
                                    </div>
                                </div>
                                <!-- //ads section -->
                                <!-- Open section to Continue loop product  -->
                                    <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mt-4">
                                        <div class="row">
                                <?php
                                    continue;
                            }
                            elseif($count==12)
                            {
                                break;
                            }
                        }
                    ?>
                    
                </div>
            </div>
            <!-- //second section -->
            
        </div>
    </div>
    <!-- //product left -->