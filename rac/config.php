<?php
session_start();
$username = "";
$email = "";
$phone="";
$postcode="";
$address="";

$db= mysqli_connect("localhost","root","","mydb");// fill out database name
if($db === false)
{
   die("ERROR: Could not connect.". mysqli_connect_error());
}


//Register account
if (isset($_POST['reg_user'])) {
    // receive all input values from the form
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    $address = mysqli_real_escape_string($db, $_POST['address']);
    $birthdate = mysqli_real_escape_string($db, $_POST['birthdate']);
    $postcode = mysqli_real_escape_string($db, $_POST['postcode']);
    $state = mysqli_real_escape_string($db, $_POST['state']);
    $gender = mysqli_real_escape_string($db, $_POST['gender']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
  
    // form validation: ensure that the form is correctly filled ...
    // by adding (array_push()) corresponding error unto $errors array
    if (empty($username)) { array_push($errors, "Username is required"); }
    if (empty($email)) { array_push($errors, "Email is required"); }
    if (empty($postcode)) { array_push($errors, "Postcode is required"); }
    if (empty($address)) { array_push($errors, "Address is required"); }
    if (empty($password_1)) { array_push($errors, "Password is required"); }
    if ($password_1 != $password_2) {
     array_push($errors, "The two passwords do not match");
    }
  
    // first check the database to make sure 
    // a user does not already exist with the same username and/or email
    $check = mysqli_query($db,"SELECT * FROM customer WHERE User_email = '$email'");
    // $user = mysqli_fetch_assoc($result);
    
    if (mysqli_num_rows($check)) { // if user exists
      exit('This email is already being used');
    }

       $password = ($password_1);//encrypt the password before saving in the database
  
       $query = "INSERT INTO customer (User_name,User_phone,User_address,User_postcode,User_email,User_password,User_state,User_birthdate,User_gender) VALUES ('$username', '$phone', '$address', '$postcode', '$email', '$password', '$state','$birthdate', '$gender')";
       mysqli_query($db, $query);
       $query2 ="SELECT * FROM customer WHERE User_email = '$email' and User_password = '$password'";
       $result = mysqli_query($db,$query2);
       if(mysqli_num_rows ($result)== 1){
        $row = mysqli_fetch_array($result);
    
        $_SESSION['User_ID'] = $row ;
        $username = $row['User_name'];
        $subject = "Account Creation Notification";
			  $message = "Dear ".$username.", \n\n You have succesful registered with RAC Car Accessories Online System !\n\n Now you can shop with us with your account!\n\n Thank you very much for choosing us !";
			  $from = 'From: RAC Car Accessories <raccar0825@gmail.com>' . "\r\n";
			
        mail($email,$subject,$message,$from);	
       
        header("location:index2.php");
       }
      //  $row = mysqli_fetch_array($result);
      //  $_SESSION['User_ID'] = $row ;
  }
 
 //Login User
 if (isset($_POST['login_user'])) 
 {
  // session_start();
   $email = ($_POST['email']);
   $password = ($_POST['password']);
   $sql = "SELECT * FROM customer WHERE User_email = '$email' and User_password = '$password'";

   $result = mysqli_query($db,$sql);

   if(mysqli_num_rows ($result)== 1){
    $row = mysqli_fetch_array($result);

    $_SESSION['User_ID'] = $row ;
  
     header("location:index2.php");
   }else{
    die(header('refresh: 3; url=index.php').'Invalid Credentials, wait 3 seconds or just click <a href="index.php">HERE</a> to check again.');
   }
 }


 ?>