<script src="path/to/sweet-alert.min.js"></script>
	<script>
	//if not tick also can alert, need ask pern
	function myfun() {
		alert("Succesful, You can Login now !");
    }
	</script>
	<!-- top-header -->
	<div class="agile-main-top">
		<div class="container-fluid">
			<div class="row main-top-w3l py-2">
				<div class="col-lg-4 header-most-top">
					<p class="text-white text-lg-left text-center">Offer Zone Top Deals & Discounts
						<i class="fas fa-shopping-cart ml-1"></i>
					</p>
				</div>
				<div class="col-lg-8 header-right mt-lg-0 mt-2">
					<!-- header lists -->
					<ul>
						<li class="text-center border-right text-white">
							<a class="play-icon popup-with-zoom-anim text-white" href="#small-dialog1">
								<i class="fas fa-map-marker mr-2"></i>Select Location</a>
						</li>
						<li class="text-center border-right text-white">
							<a href="#" data-toggle="modal" data-target="#exampleModal" class="text-white">
								<i class="fas fa-truck mr-2"></i>Track Order</a>
						</li>
						<li class="text-center border-right text-white">
							<i class="fas fa-phone mr-2"></i> 06 - 953 9137
						</li>
						<li class="text-center border-right text-white">
							<a href="#" data-toggle="modal" data-target="#exampleModal" class="text-white">
								<i class="fas fa-sign-in-alt mr-2"></i> Log In </a>
						</li>
						<li class="text-center text-white">
							<a href="#" data-toggle="modal" data-target="#exampleModal2" class="text-white">
								<i class="fas fa-sign-out-alt mr-2"></i>Register </a>
						</li>
					</ul>
					<!-- //header lists -->
				</div>
			</div>
		</div>
	</div>

	<!-- Button trigger modal(select-location) -->
	<div id="small-dialog1" class="mfp-hide">
		<div class="select-city">
			<h3>
				<i class="fas fa-map-marker"></i> Please Select Your Location</h3>
			<select class="list_of_cities">
				<optgroup label="Popular Cities">
					<option selected style="display:none;color:#eee;">Select City</option>
					<option>Kuala Lumpur</option>
					<option>Melaka</option>
					<option>Muar</option>
				</optgroup>
				<optgroup label="Negeri Sembilan">
					<option>Seremban</option>
				</optgroup>
				<optgroup label="Malacca">
					<option>Malacca City</option>
				</optgroup>
				<optgroup label="Johor">
					<option>Johor Bahru</option>
					<option>Tangkak</option>
					<opiton>Bukit Gambir</opiton>
					<option>Muar</option>
					<option>Bukit Pasir</option>
				</optgroup>
				<optgroup label="Kedah">
					<option>Alor Setar</option>
				</optgroup>
				<optgroup label="Kelantan">
					<option>Kota Bahru</option>
				</optgroup>
				<optgroup label="Pahang">
					<option>Kuantan</option>
				</optgroup>
				<optgroup label="Perlis">
					<option>Kangar</option>
				</optgroup>
				<optgroup label="Perak">
					<option>Ipoh</option>	
				</optgroup>
				<optgroup label="Selangor">
					<option>Shah Alam</option>
				</optgroup>
				<optgroup label="Penang">
					<option>George Town</option>
				</optgroup>
				<optgroup label="Terengganu">
					<option>Kuala Terengganu</option>
				</optgroup>
				<optgroup></optgroup>
				<optgroup label="East Malaysia ">
					<optgroup label="Sarawak">
						<option>Kuching</option>
						<option>Sibu</option>
					</optgroup>
					<optgroup label="Sabah">
						<option>Sandakan</option>
					</optgroup>
				</optgroup>
			</select>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //shop locator (popup) -->

	<!-- modals -->
	<!-- log in -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center">Log In</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="#" method="post">
						<div class="form-group">
							<label class="col-form-label">Your Email</label>
							<input type="text" class="form-control" placeholder=" " name="email" required="">
						</div>
						<div class="form-group">
							<label class="col-form-label">Password</label>
							<input type="password" class="form-control" id="password0" name="password" required="">
							<input type="checkbox" onclick="TogglePass0()">Show Password
						</div>
						<div class="right-w3l">
							<input type="submit" id="login" class="form-control" name="login_user" value="Log in">
						</div>
						
						<div class="sub-w3l">
							<div class="custom-control custom-checkbox mr-sm-2">
								<input type="checkbox" class="custom-control-input" id="customControlAutosizing">
								<label class="custom-control-label" for="customControlAutosizing">Remember me?</label>
							</div>
						</div>
						<p class="text-center dont-do mt-3">
							<a href="forgetpass.php">Forget Password ?</a>	
							</p>
						<p class="text-center dont-do mt-3">Don't have an account?
							<a href="#" data-toggle="modal" data-target="#exampleModal2">
								Register Now</a> 
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- register -->
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Register</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="#" method="post">
						<div class="form-group">
							<label class="col-form-label">Your Name</label>
							<input type="text" value="<?php echo $username; ?>" class="form-control" placeholder=" " name="username" required="">

						</div>
						<div class="form-group">
							<label class="col-form-label">Your Phone</label>
							<input type="tel" value="<?php echo $phone; ?>" class="form-control" placeholder=" " name="phone" required="" pattern="[0-9]{3}-[0-9]{7-8}">
						
						</div>
						<div class="form-group">
							<label class="col-form-label">Your Address</label>
							<input type="text" value="<?php echo $address; ?>" class="form-control" placeholder=" " name="address" required="">
						
						</div>
						<div class="form-group">
							<label class="col-form-label">Postcode</label>
							<input type="text" value="<?php echo $postcode; ?>" class="form-control" placeholder=" " name="postcode" required="" pattern="[0-9]{5}" >
						
						</div>
						<div class="form-group">
							<label class="col-form-label">Birth Date</label>
							<input id="datePickerId" type="date" value="<?php echo $birthdate; ?>" class="form-control" placeholder=" " name="birthdate" required="">
						
						</div>
						<div class="form-group">
							<label for ="gender" class="col-form-label">Gender</label>
							<select id="gender" name="gender" value="<?php echo $gender; ?>">
								<option value="male">Male</option>
								<option value="female">Female</option>
								<option value="other">Other</option>
							</select>
							
						</div>
						<div class="form-group">
							<label for ="state" class="col-form-label">State</label>
							<select id="gender" name="state" value="<?php echo $state; ?>">
								<optgroup label="Popular Cities">
									<option selected style="display:none;color:#eee;">Select City</option>
									<option>Kuala Lumpur</option>
									<option>Melaka</option>
									<option>Muar</option>
								</optgroup>
								<optgroup label="Johor">
									<option>Johor Bahru</option>
									<option>Tangkak</option>
									<option>Muar</option>
								</optgroup>
								<optgroup label="Selangor">
									<option>Kuala Lumpur</option>
									<option>Shah Alam</option>
									<option>Klang</option>
								</optgroup>
								<optgroup label="Melaka">
									<option>Melaka</option>
								</optgroup>
								<optgroup label="Penang">
									<option>George Town</option>
								</optgroup>
								<optgroup label="Terengganu">
									<option>Kuala Terengganu</option>
								</optgroup>
							</select>
							
						</div>
						<div class="form-group">
							<label class="col-form-label">Email</label>
							<input type="email" value="<?php echo $email; ?>" class="form-control" placeholder=" " name="email" required="">
						
						</div>
						<div class="form-group">
							<label class="col-form-label">Password</label>
							<input type="password"  class="form-control" placeholder=" " name="password_1" id="password1" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required="" > 
							<input type="checkbox" onclick="TogglePass1()">Show Password
						</div><div id="message">
  							<h5>Password must contain the following:</h5>
 						    <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
 							 <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
  							<p id="number" class="invalid">A <b>number</b></p>
  							 <p id="length" class="invalid">Minimum <b>8 characters</b></p>
						</div>
						<div class="form-group">
							<label class="col-form-label">Confirm Password</label>
							<input type="password" class="form-control" placeholder=" " name="password_2" id="password2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  required="" >
							<input type="checkbox" onclick="TogglePass2()">Show Password
						</div>
						<div class="slidercaptcha card">
                    <div class="card-header">
                        <span>Drag To Verify</span>
                    </div>
			
                    <div class="card-body"><div id="captcha"></div></div>
			
                </div>
						<div class="right-w3l">
							<input type="submit" onclick="return myfun()" class="form-control" name="reg_user" value="Register">
						</div>
						<div class="sub-w3l">
							<div class="custom-control custom-checkbox mr-sm-2">
								
								<input type="checkbox" class="custom-control-input" id="customControlAutosizing2" required="">
								<label class="custom-control-label" for="customControlAutosizing2">I Accept to the Terms & Conditions</label>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--//register-->
	<!-- //modal -->
	<!-- //top-header -->

	<!-- header-bottom-->
	<div class="header-bot">
		<div class="container">
			<div class="row header-bot_inner_wthreeinfo_header_mid">
				<!-- logo -->
				<div class="col-md-3 logo_agile">
					<h1 class="text-center">
						<a href="index.php" class="font-weight-bold font-italic">
							<img src="images/logo3.jpg" alt=" " class="img-fluid" style="padding-top:7px">&nbsp&nbsp&nbspAccessories
						</a>
					</h1>
				</div>
				<!-- //logo -->
				<!-- header-bot -->
				<div class="col-md-9 header mt-4 mb-md-0 mb-4">
					<div class="row">
						<!-- search -->
						<div class="col-10 agileits_search">
							<form class="form-inline" action="#" method="post">
								<input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search" required>
								<button class="btn my-2 my-sm-0" type="submit">Search</button>
							</form>
						</div>
						<!-- //search -->
						<!-- cart details -->
						<div class="col-2 top_nav_right text-center mt-sm-0 mt-2">
							<div class="wthreecartaits wthreecartaits2 cart cart box_1">
								<form action="#" method="post" class="last">
									<input type="hidden" name="cmd" value="_cart">
									<input type="hidden" name="display" value="1">
									<button class="btn w3view-cart" type="submit" name="submit" value="">
										<i class="fas fa-cart-arrow-down"></i>
									</button>
								</form>
							</div>
						</div>
						<!-- //cart details -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- shop locator (popup) -->
	<!-- //header-bottom -->
	<!-- navigation -->
	<div class="navbar-inner">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="agileits-navi_search">
					<form action="#" method="post">
						<select id="agileinfo-nav_search" name="agileinfo_search" class="border" required="">
							<option value="">All Categories</option>
							<?php
								$result=mysqli_query($db,"SELECT * from category");
								while($row=mysqli_fetch_assoc($result))
								{
							?>
							<option value="<?php echo $row['Category_ID'] ?>"><?php echo $row['Category_name'] ?></option>
							<?php } ?>
						</select>
					</form>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto text-center mr-xl-5">
						<li class="nav-item active mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link" href="index.php">Home
								<span class="sr-only">(current)</span>
							</a>
						</li>
						<li class="nav-item dropdown mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Categories
							</a>
							<div class="dropdown-menu">
								<div class="agile_inner_drop_nav_info p-4">
									<!-- <h5 class="mb-3">Mobiles, Computers</h5> -->
									<div class="row">
										<div class="col-sm-6 multi-gd-img">
											<ul class="multi-column-dropdown">
												<?php 
													$result2=mysqli_query($db,"SELECT * from category");
													$count=0;
													while($row2=mysqli_fetch_assoc($result2))
													{
														if($count==6)
														{
															?>
																</ul>
															</div>
															<div class="col-sm-6 multi-gd-img">
																<ul class="multi-column-dropdown">
																	<li>
																		<a href="product.php?cate_id=<?php echo $row2['Category_ID'] ?>" style="color:blue;text-decoration:underline;"><?php echo $row2['Category_name'] ?></a>
																		<ul>
																			<?php
																				$result3=mysqli_query($db,"SELECT * from subcategory where cate_ID='".$row2['Category_ID']."'");
																				while($row3=mysqli_fetch_assoc($result3))
																				{
																			?>
																			<li>
																				<a href="product.php?subcate_id=<?php echo $row3['Subcategory_ID'] ?>"><?php echo $row3['Subcategory_name'] ?></a>
																			</li>
																			<?php } ?>
																		</ul>
																	</li>
															<?php
															continue;
														}
												?>
												<li>
													<a href="product.php?cate_id=<?php echo $row2['Category_ID'] ?>" style="color:blue;text-decoration:underline;"><?php echo $row2['Category_name'] ?></a>
													<ul>
														<?php
															$result3=mysqli_query($db,"SELECT * from subcategory where cate_ID='".$row2['Category_ID']."'");
															while($row3=mysqli_fetch_assoc($result3))
															{
														?>
														<li>
															<a href="product.php?subcate_id=<?php echo $row3['Subcategory_ID'] ?>"><?php echo $row3['Subcategory_name'] ?></a>
														</li>
														<?php } ?>
													</ul>
												</li>
												<?php 
												$count++;	
												} 
												?>
											</ul>
										</div>
									
									</div>
								</div>
							</div>
						</li>
						
						<li class="nav-item mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link" href="about.php">About Us</a>
						</li>
						<!-- <li class="nav-item mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link" href="product.php">New Arrivals</a>
						</li> -->
						<li class="nav-item dropdown mr-lg-2 mb-lg-0 mb-2">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Pages
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="checkout.php">Checkout Page</a>
								<a class="dropdown-item" href="payment.php">Payment Page</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="faqs2.php">Q&A</a>
								<a class="dropdown-item" href="help2.php">Help Page</a>
								<a class="dropdown-item" href="privacy2.php">Privacy & Policy</a>
								<a class="dropdown-item" href="terms2.php">Terms & Conditions</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="contact.php">Contact Us</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	<script>
        $('#captcha').sliderCaptcha({
            repeatIcon: 'fa fa-redo',
			rules:{
				required : true,
			}
        });
    </script>
	<script>
function TogglePass0() {
  var x = document.getElementById("password0");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function TogglePass1() {
  var y = document.getElementById("password1");
  if (y.type === "password") {
    y.type = "text";
  } else {
    y.type = "password";
  }
}
function TogglePass2() {
  var z = document.getElementById("password2");
  if (z.type === "password") {
    z.type = "text";
  } else {
    z.type = "password";
  }
}
</script>
<!-- Password Strength -->
<script>
var myInput = document.getElementById("password1");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  
  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}
</script>
	
	<!-- //navigation -->