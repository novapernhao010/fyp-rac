<?php 
//include("config.php"); 
?>
<!-- product right -->
<div class="col-lg-3 mt-lg-0 mt-4 p-lg-0">
    <div class="side-bar p-sm-4 p-3">
        <div class="search-hotel border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Search Here..</h3>
            <form action="#" method="post">
                <input type="search" placeholder="Product name..." name="search" required="">
                <input type="submit" value=" ">
            </form>
        </div>
        <!-- price -->
        <div class="range border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Price</h3>
            <div class="w3l-range">
                <ul>
                    <li>
                        <a href="#">Under RM100</a>
                    </li>
                    <li class="my-1">
                        <a href="#">RM100-RM200</a>
                    </li>
                    <li>
                        <a href="#">RM200-RM300</a>
                    </li>
                    <li class="my-1">
                        <a href="#">RM300-RM400</a>
                    </li>
                    <li>
                        <a href="#">RM400-RM500</a>
                    </li>
                    <li class="mt-1">
                        <a href="#">Over Rm500</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- //price -->
        <!-- discounts -->
        <div class="left-side border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Discount</h3>
            <ul>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">5% or More</span>
                </li>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">10% or More</span>
                </li>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">20% or More</span>
                </li>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">30% or More</span>
                </li>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">50% or More</span>
                </li>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">60% or More</span>
                </li>
            </ul>
        </div>
        <!-- //discounts -->
        <!-- reviews -->
        <div class="customer-rev border-bottom left-side py-2">
            <h3 class="agileits-sear-head mb-3">Customer Review</h3>
            <ul>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <span>5.0</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <span>4.0</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half"></i>
                        <span>3.5</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <span>3.0</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half"></i>
                        <span>2.5</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- //reviews -->
        <!-- product -->
        <?php 
            $result=mysqli_query($db,"SELECT * from category");
        ?>
        <div class="left-side border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Category</h3>
            <ul>
                <?php
                    while($row=mysqli_fetch_assoc($result))
                    {
                ?>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span"><?php echo $row['Category_name'] ?></span>
                </li>
                <?php
                    }
                ?>
            </ul>
        </div>
        <!-- //product -->
        <!-- delivery -->
        <div class="left-side border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">Cash On Delivery</h3>
            <ul>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">Eligible for Cash On Delivery</span>
                </li>
            </ul>
        </div>
        <!-- //delivery -->
        <!-- arrivals -->
        <div class="left-side border-bottom py-2">
            <h3 class="agileits-sear-head mb-3">New Arrivals</h3>
            <ul>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">Last 30 days</span>
                </li>
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">Last 90 days</span>
                </li>
            </ul>
        </div>
        <!-- //arrivals -->
        <!-- best seller -->
        <!-- <div class="f-grid py-2">
            <h3 class="agileits-sear-head mb-3">Best Seller</h3>
            <div class="box-scroll">
                <div class="scroll">
                    <div class="row">
                        <div class="col-lg-3 col-sm-2 col-3 left-mar">
                            <img src="images/k1.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-lg-9 col-sm-10 col-9 w3_mvd">
                            <a href="">Samsung Galaxy On7 Prime (Gold, 4GB RAM + 64GB Memory)</a>
                            <a href="" class="price-mar mt-2">$12,990.00</a>
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="col-lg-3 col-sm-2 col-3 left-mar">
                            <img src="images/k2.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-lg-9 col-sm-10 col-9 w3_mvd">
                            <a href="">Haier 195 L 4 Star Direct-Cool Single Door Refrigerator</a>
                            <a href="" class="price-mar mt-2">$12,499.00</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-2 col-3 left-mar">
                            <img src="images/k3.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-lg-9 col-sm-10 col-9 w3_mvd">
                            <a href="">Ambrane 13000 mAh Power Bank (P-1310 Premium)</a>
                            <a href="" class="price-mar mt-2">$1,199.00 </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- //best seller -->
    </div>
    <!-- //product right -->