<!DOCTYPE html>
<html lang="zxx">


<?php include("html_head.php"); ?>

<style>
input[type=email],input[type=text],input[type=password]{
    width:90%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: none;
    border-bottom: 2px solid red;
}
input[type=submit]{
    height: 1.3cm;
    background-color: #008CBA;;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    border: none;
    color: white;
    width : 50%;
}
.wrapper {
    text-align: center;
}
</style>
<body>
	<?php include("header.php"); ?>
    <div class="main-agile">
	<!-- main -->
    <div class="services-breadcrumb">
		<div class="agile_inner_breadcrumb">
			<div class="container">
				<ul class="w3_short">
					<li>
						<a href="index.php">Home</a>
						<i>|</i>
					</li>
					<li><span style="font-style:italic;">Reset Password</span></li>
				</ul>
			</div>
		</div>
	</div>
    <div class="contact py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>Reset Your Password Here</span>
			</h3>
		
					<div class="signin-form recover-password">
						<form action="" name="forgetform" method="post">
							<input type="email" placeholder="Your Email" name="email" size="50" value="<?php if(isset($_POST["email"])) echo $_POST["email"]; ?>" required />
							<input  placeholder="Pin Code" type="text" name="pin" size="50" value="<?php if(isset($_POST["pin"])) echo $_POST["pin"]; ?>" required autocomplete="off">

						
							<input type="password" id="userpass" name="password" placeholder="New Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required autofocus oninput="CheckPassword()" maxlength="30">
							
                            <span id="result2"></span>
							
						
							<input type="password" id="userConfirmPassword" name="re-password"  placeholder="Re-enter Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" autofocus maxlength="30" oninput="return Validatepass()">
							
							<span id="result3"></span>
							<br>
                            <br>
                            <div class="wrapper">
							<input type="submit" class="send" name="pass" value="Submit and Change">
                            </div>
                            <br>
                            <br>
						</form>
					
					
				
			</div>
			</div>
	<?php

if(isset($_POST["pass"]))
{ 
	$cemail = $_POST['email'];
	$pin = $_POST['pin'];
	$new = $_POST['password'];
	$conf = $_POST["re-password"];
	$currdate = date('H:i:s');

		$email_check = mysqli_query($db, "select * from customer where User_Email= '$cemail' and User_secure_date >= '$currdate' and User_pincode = '$pin'");
		$count = mysqli_num_rows($email_check);
		$row = mysqli_fetch_assoc($email_check);
		
		if($count != 0)
		{
				if($new == $conf)
					{
						mysqli_query($db,"update customer set User_password = '$new' where User_email = '$cemail'");
					?>
					<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
					<script type="text/javascript">
						swal({title:"<?php echo 'Password Change Successful!'?>",text:"Kindly Login to your account by using new password.",icon:"success"}).then(function(){window.location.href="index.php";});
					</script>
					<?php
				   }
				   else
				   {
					   ?>
				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>		
				<script type="text/javascript">
					swal({
						   title: "You have entered wrong password combination !",
						   text:"New password and Confirm Password must be the ssame !",
						   icon:"error"
					   });
				</script>
			<?php
				   }	
		}
		else
		{
			 ?>
			<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>		
			<script type="text/javascript">
				swal({
					title: "Failed!",
					text:"Please key in correct E-mail or Security Pin.",
					icon:"error"
					});
			</script>
	<?php
		}   
	}
?>
</body>
</html>