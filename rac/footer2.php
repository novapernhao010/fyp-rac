<?php 
//include("config.php"); 
?>
<!-- middle section -->
<div class="join-w3l1 py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<div class="row">
				<div class="col-lg-6">
					<div class="join-agile text-left p-4">
						<div class="row">
							<div class="col-sm-7 offer-name">
								<h6>Smooth, Clear & Loud Audio</h6>
								<h4 class="mt-2 mb-3">Infinity Car Speakers – Kappa & Reference Series</h4>
								<p>Sale up to 25% off all in store</p>
							</div>
							<div class="col-sm-5 offerimg-w3l">
								<img src="images/speaker.jpeg" alt="" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 mt-lg-0 mt-5">
					<div class="join-agile text-left p-4">
						<div class="row ">
							<div class="col-sm-7 offer-name">
								<h6>Car Polish Machine</h6>
								<h4 class="mt-2 mb-3">Branded Car Polish Machine</h4>
								<p>Free shipping order over $1000</p>
							</div>
							<div class="col-sm-5 offerimg-w3l">
								<img src="images/polish.jpg" alt="" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- middle section -->
<!-- footer -->
<footer>
		<div class="footer-top-first">
			<div class="container py-md-5 py-sm-4 py-3">
				<!-- footer first section -->
				<h2 class="footer-top-head-w3l font-weight-bold mb-2">Car Accessories :</h2>
				<p class="footer-main mb-4">
					If you're considering a new car accessories, looking for a powerful new car stereo or shopping for a new Car Polish Machine, we make it easy to
					find exactly what you need at a price you can afford. We offer Every Day Low Prices on car speakers, car polish machine, bumper guard, spoiler,
					side sill plate, alarm & security system, car cares, engine performance, cooling system, chasis & suspension and more.</p>
				<!-- //footer first section -->
				<!-- footer second section -->
				<div class="row w3l-grids-footer border-top border-bottom py-sm-4 py-3">
					<div class="col-md-4 offer-footer">
						<div class="row">
							<div class="col-4 icon-fot">
								<i class="fas fa-dolly"></i>
							</div>
							<div class="col-8 text-form-footer">
								<h3>Free Shipping</h3>
								<p>on orders over RM1000</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 offer-footer my-md-0 my-4">
						<div class="row">
							<div class="col-4 icon-fot">
								<i class="fas fa-shipping-fast"></i>
							</div>
							<div class="col-8 text-form-footer">
								<h3>Fast Delivery</h3>
								<p>Malaysia</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 offer-footer">
						<div class="row">
							<div class="col-4 icon-fot">
								<i class="far fa-thumbs-up"></i>
							</div>
							<div class="col-8 text-form-footer">
								<h3>Big Choice</h3>
								<p>of Products</p>
							</div>
						</div>
					</div>
				</div>
				<!-- //footer second section -->
                <!-- footer payment section -->
				<div class="sub-some child-momu mt-4">
					<h5 class="font-weight-bold mb-3">Payment Method</h5>
					<ul>
						<li>
							<img src="images/pay2.png" alt="">
						</li>
						<li>
							<img src="images/pay1.png" alt="">
						</li>
						<li>
							<img src="images/maybank.png" width="100" length="100 " alt="">
						</li>
						<li>
							<img src="images/cimb.jpg" width="100" length="100 "alt="">
						</li>
						<li>
							<img src="images/public.png" width="100" length="100 " alt="">
						</li>
						<li>
							<img src="images/rhb.jpg" width="100" length="100 " alt="">
						</li>
						<li>
							<img src="images/hsbc.png" width="100" length="100 " alt="">
						</li>
						<li>
							<img src="images/pay7.png" width="100" length="100 " alt="">
						</li>
						<li>
							<img src="images/pay8.png" width="100" length="100 " alt="">
						</li>
					</ul>
				</div>
				<!-- //footer payment section -->
			</div>
		</div>
		<!-- footer third section -->
		<div class="w3l-middlefooter-sec">
			<div class="container py-md-5 py-sm-4 py-3">
				<div class="row footer-info w3-agileits-info">
					<!-- footer categories -->
					<div class="col-md-3 col-sm-6 footer-grids">
						<h3 class="text-white font-weight-bold mb-3">Categories</h3>
						<ul>
							<li class="mb-3">
								<a href="product2.php">Car Accessories </a>
							</li>
							<li class="mb-3">
								<a href="product2.php">Engine Performance</a>
							</li>
							<li class="mb-3">
								<a href="product2.php">Replacement Parts</a>
							</li>
							<li class="mb-3">
								<a href="product2.php">4x4</a>
							</li>
						</ul>
					</div>
					<!-- //footer categories -->
					<!-- quick links -->
					<div class="col-md-3 col-sm-6 footer-grids mt-sm-0 mt-4">
						<h3 class="text-white font-weight-bold mb-3">Quick Links</h3>
						<ul>
							<li class="mb-3">
								<a href="about2.php">About Us</a>
							</li>
							<li class="mb-3">
								<a href="contact2.php">Contact Us</a>
							</li>
							<li class="mb-3">
								<a href="help2.php">Help</a>
							</li>
							<li class="mb-3">
								<a href="faqs2.php">Faqs</a>
							</li>
							<li class="mb-3">
								<a href="terms2.php">Terms of use</a>
							</li>
							<li>
								<a href="privacy2.php">Privacy Policy</a>
							</li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-6 footer-grids mt-md-0 mt-4">
						<h3 class="text-white font-weight-bold mb-3">Get in Touch</h3>
						<ul>
							<li class="mb-3">
								<i class="fas fa-map-marker"></i> D02-03 Jalan Ixora, Car Accessories System</li>
							<li class="mb-3">
								<i class="fas fa-mobile"></i> 06 - 953 9137 </li>
							<li class="mb-3">
								<i class="fas fa-phone"></i> fax 06 - 953 9137 </li>
							<li class="mb-3">
								<i class="fas fa-envelope-open"></i>
								<a href="mailto:example@mail.com"> onlinecar@hotmail.com </a>
							</li>
							<li>
								<i class="fas fa-envelope-open"></i>
								<a href="mailto:example@mail.com"> onlinecar2@hotmail.com </a>
							</li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-6 footer-grids w3l-agileits mt-md-0 mt-4">
						<!-- newsletter -->
						<!-- <h3 class="text-white font-weight-bold mb-3">Newsletter</h3>
						<p class="mb-3">Free Delivery on your first order!</p>
						<form action="#" method="post">
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Email" name="email" required="">
								<input type="submit" value="Go">
							</div>
						</form> -->
						<!-- //newsletter -->
						<!-- social icons -->
						<div class="footer-grids  w3l-socialmk mt-3">
							<h3 class="text-white font-weight-bold mb-3">Follow Us on</h3>
							<div class="social">
								<ul>
									<li>
										<a class="icon fb" href="#">
											<i class="fab fa-facebook-f"></i>
										</a>
									</li>
									<li>
										<a class="icon tw" href="#">
											<i class="fab fa-twitter"></i>
										</a>
									</li>
									<li>
										<a class="icon gp" href="#">
											<i class="fab fa-google-plus-g"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- //social icons -->
					</div>
				</div>
				<!-- //quick links -->
			</div>
		</div>
		<!-- //footer third section -->

	</footer>
	<!-- //footer -->
	<!-- copyright -->
	<div class="copy-right py-3">
		<div class="container">
			<p class="text-center text-white">Copyright © 2020 RAC Accessories. All Rights Reserved
			</p>
		</div>
	</div>
	<!-- //copyright -->