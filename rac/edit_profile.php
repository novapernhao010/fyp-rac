<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<?php include("html_head.php"); 
	   ob_start();
?>

<style>
#box1
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
	background-color:white;
	color:black;
}
#box2{
	height:150px;
	font-size:12pt;
	border: 2px double grey;
  	border-radius: 4px;
	color:black;
}
#box3
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
	background-color:white;
	color:black;
}
input:focus {
  
  border:2px double blue;
}
select {
        width: 540px;
		height:80px;
		font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
    }
    select:focus {
        min-width: 150px;
        width: 540px;
    }    
</style>

<body>
	<?php include("header2.php"); ?>
	<!-- page -->
	<?php
		if(isset($_SESSION["User_ID"])){
			$userData = $_SESSION["User_ID"];

			$username = $userData["User_name"];
			$uemail = $userData["User_email"];
			$phone = $userData["User_phone"];
			$address = $userData["User_address"];
			$state = $userData["User_state"];
			$postcode = $userData["User_postcode"];
			$gender = $userData["User_gender"];
			$password = $userData["User_password"];
		}
	?>
	<div class="services-breadcrumb">
		<div class="agile_inner_breadcrumb">
			<div class="container">
				<ul class="w3_short">
					<li>
						<a href="index.php">Home</a>
						<i>|</i>
					</li>
					<li><span style="font-style:italic;">Edit Profile</span></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //page -->

	<!-- contact -->
	<div class="contact py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>Edit </span><span>P</span><span style="font-style:italic;">rofile</span>
			</h3>
			<!-- form -->

			<form action="#" method="post">
				<div class="contact-grids1 w3agile-6">
					<div class="row">
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Name</label>
							<input type="text" class="form-control" id="box1" name="name"  placeholder="<?php echo $username; ?>" value="<?php echo $username; ?>" required>
						</div>
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">E-mail</label>
							<input type="text" class="form-control"  id="box1" name="email"  placeholder="<?php echo $userData['User_email']; ?>"  value="<?php echo $uemail; ?>" required>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Phone</label>
						<input type="text" class="form-control"  id="box1" name="phone"  placeholder="<?php echo $userData['User_phone']; ?>" value="<?php echo $phone; ?>" pattern="[0-9]{3}-[0-9]{7-8}" required>
					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Gender</label><br>
						<select id="gender" name="gender" style="text-transform:capitalize;" value="<?php echo $gender; ?>">
								<option value="male">Male</option>
								<option value="female">Female</option>
								<option value="other">Other</option>
							</select>
					</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Postcode</label>
						<input type="text" class="form-control"  id="box1" name="postcode"  placeholder="<?php echo $userData['User_postcode']; ?>"  value="<?php echo $postcode; ?>" pattern="[0-9]{5}" required>
					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">State</label>
						<select id="state" name="state" placeholder="<?php echo $userData['User_state']; ?>" value="<?php echo $state; ?>">
								<optgroup label="Popular Cities">
									<!-- <option selected style="display:none;color:#eee;">Select City</option> -->
									<option>Kuala Lumpur</option>
									<option>Melaka</option>
									<option>Muar</option>
								</optgroup>
								<optgroup label="Johor">
									<option>Johor Bahru</option>
									<option>Tangkak</option>
									<option>Muar</option>
								</optgroup>
								<optgroup label="Selangor">
									<option>Kuala Lumpur</option>
									<option>Shah Alam</option>
									<option>Klang</option>
								</optgroup>
								<optgroup label="Melaka">
									<option>Melaka</option>
								</optgroup>
								<optgroup label="Penang">
									<option>George Town</option>
								</optgroup>
								<optgroup label="Terengganu">
									<option>Kuala Terengganu</option>
								</optgroup>
							</select>
					</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Password</label>
							<input type="password" class="form-control" id="box1" name="password"  value="<?php echo $password; ?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
						</div>
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Confirm Password</label>
							<input type="password" class="form-control"  id="box1" name="conpassword"  value="<?php echo $password; ?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
						</div>
					</div>
					<div class="contact-me animated wow slideInUp form-group">
						<label class="col-form-label">Address</label>
						<input type="text" name="address" id="box2" class="form-control"  placeholder="<?php echo $userData['User_address']; ?>"  value="<?php echo $address; ?>" required>
					</div>
					<div class="contact-form">
						<input type="submit" value="Update Profile" name="edit_profile" >
					</div>
				</div>
			</form>
			<!-- //form -->
		</div>
	</div>

	<?php include("footer2.php"); ?>

	<!-- js-files -->
	<!-- jquery -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- //jquery -->

	<!-- nav smooth scroll -->
	<script>
		$(document).ready(function () {
			$(".dropdown").hover(
				function () {
					$('.dropdown-menu', this).stop(true, true).slideDown("fast");
					$(this).toggleClass('open');
				},
				function () {
					$('.dropdown-menu', this).stop(true, true).slideUp("fast");
					$(this).toggleClass('open');
				}
			);
		});
	</script>
	<!-- //nav smooth scroll -->

	<!-- popup modal (for location)-->
	<script src="js/jquery.magnific-popup.js"></script>
	<script>
		$(document).ready(function () {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});

		});
	</script>
	<!-- //popup modal (for location)-->

	<!-- cart-js -->
	<script src="js/minicart.js"></script>
	<script>
		paypals.minicarts.render(); //use only unique class names other than paypals.minicarts.Also Replace same class name in css and minicart.min.js

		paypals.minicarts.cart.on('checkout', function (evt) {
			var items = this.items(),
				len = items.length,
				total = 0,
				i;

			// Count the number of each item in the cart
			for (i = 0; i < len; i++) {
				total += items[i].get('quantity');
			}

			if (total < 3) {
				alert('The minimum order quantity is 3. Please add more to your shopping cart before checking out');
				evt.preventDefault();
			}
		});
	</script>
	<!-- //cart-js -->

	<!-- password-script -->
	<script>
		window.onload = function () {
			document.getElementById("password1").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}

		function validatePassword() {
			var pass2 = document.getElementById("password2").value;
			var pass1 = document.getElementById("password1").value;
			if (pass1 != pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');
			//empty string means no validation error
		}
	</script>
	<!-- //password-script -->

	<!-- smoothscroll -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- //smoothscroll -->

	<!-- start-smooth-scrolling -->
	<script src="js/move-top.js"></script>
	<script src="js/easing.js"></script>
	<script>
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script>
		$(document).ready(function () {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->

	<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</body>
</html>
<?php
if (isset($_POST['edit_profile'])) {

$username = $_POST["name"];
$email = $_POST["email"];
$phone = $_POST["phone"];
$address = $_POST["address"];
$gender = $_POST["gender"];
$state = $_POST["state"];
$postcode = $_POST["postcode"];
$password = $_POST["password"];
$conpassword = $_POST["conpassword"];

$check = mysqli_query($db,"SELECT * FROM customer WHERE User_email = '$email'");

if($email != $userData["User_email"])
{
  ?>
		<script type="text/javascript">
		swal({title: "Email has been Use!",
			text: "Please enter another email!",
			icon: "error",
			button: "Retry"}).then(function(){window.location.href = "edit_profile.php";});
		</script>
	  <?php	
}else{
	if($password != $conpassword){
		?>
				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>		
				<script type="text/javascript">
					swal({
					title: "Password Does Not Match	!",
					text:"Please Try Again!",
					icon:"error"
					});
				</script>
		<?php
	}else{	
 		$update = mysqli_query($db,"UPDATE customer SET User_name='$username',User_email ='$email', User_password = '$password',User_phone='$phone',User_address = '$address',User_state = '$state',User_postcode='$postcode',User_gender ='$gender' WHERE User_Email = '$uemail'");
  	?>
		<script type="text/javascript">
		r = confirm("Do you want to edit your profile ?");
		if(r==true){
		  swal({title: "Edit Successful!",
			  icon: "success",
			  button: "Relogin"}).then(function(){window.location.href ="index.php";<?php ob_end_flush(); ?>});
		}else{
			swal({
				title : "Nothing change",
				icon  : "warning"
			});
			}
		</script>
	  <?php
	}
}
}
?>