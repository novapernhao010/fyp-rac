<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php");
      ob_start();
?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Add New Admin</h3>
        <a href="manage_admin.php" style="color:white"><button type="button" class="btn btn-round btn-danger"><i class="fa fa-arrow-left"></i> BACK</button></a>
        <div class="row">
          <div class="col-lg-12">
            <div class="form-panel">
              <form class="form-horizontal style-form" method="post" enctype="multipart/form-data" action="">
                <div class="form-group">
                  <label class="col-lg-2 control-label">Name</label>
                  <div class="col-lg-6">
                    <input type="text" placeholder=" " class="form-control" name="name" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Gender</label>
                  <div class="col-lg-2">
                    <div class="radio">
                        <label>
                            <input type="radio" name="gender" value="Male" checked>Male
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="gender" value="Female" >Female
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Phone</label>
                  <div class="col-lg-6">
                    <input type="text" placeholder=" " class="form-control" name="phone" required pattern="[0-9]{3}-[0-9]{7-8}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Address</label>
                  <div class="col-lg-6">
                    <input type="text" placeholder=" " class="form-control" name="address">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Email</label>
                  <div class="col-lg-6">
                    <input type="email" class="form-control" placeholder=" " name="email" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Password</label>
                  <div class="col-lg-6">
                    <input type="password" id="myInput" placeholder=" " class="form-control" name="password" required>
                    <!-- An element to toggle between password visibility -->
                    <input type="checkbox" onclick="myFunction()">Show Password
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-theme" type="submit" name="savebtn">Save</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /col-md-12 -->
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script src="lib/advanced-form-components.js"></script>

  <script>
    function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
    }
    </script>
</body>

</html>
<?php
//add admin
if(isset($_POST["savebtn"])) 	
{
  $name = $_POST["name"];
  $gender = $_POST["gender"];  	
  $phone = $_POST["phone"];
  $address = $_POST["address"];  
  $email = $_POST["email"]; 
  $password = $_POST["password"]; 
  $level = "A" ;
	
	mysqli_query($db,"INSERT INTO admin(Admin_name,Admin_gender,Admin_phone,Admin_address,Admin_email,Admin_password,Admin_level) 
  VALUES('$name','$gender','$phone','$address','$email','$password','$level')");
  
  
  header("refresh:0; url=manage_admin.php");
  ob_end_flush();
}

?>