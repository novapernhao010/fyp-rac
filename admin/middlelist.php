<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php") ?>
<link href="cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" rel="stylesheet">
<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
      <h3><i class="fa fa-angle-right"></i> List of Banner</h3>
        <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <h4><i class="fa fa-angle-right"></i> List Table</h4>
              <section id="unseen">
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th>Company List</th>
                      <th class="numeric">Ads Price(Per Day)</th>
                      <th class="numeric">Date Starts</th>
                      <th class="numeric">Date End</th>
                      <th class="numeric">Title</th>
                      <th class="numeric">Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>F1 Company Repairing</td>
                      <td class="numeric">$2.00</td>
                      <td class="numeric">18-01-2020</td>
                      <td class="numeric">25-01-2020</td>
                      <td class="numeric">Car Repairing</td>
                      <td><button type="button" class="btn btn-primary btn-circle"><i class="fa fa-edit"> Edit</i></td>
                    </tr>
                  </tbody>
                </table>
              </section>
            </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>

</html>