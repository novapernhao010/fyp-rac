<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php");
      ob_start();
?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
            <?php
              if(isset($_GET["id"]))
              {
                $category_id=$_GET["id"];
                $result0=mysqli_query($db, "SELECT * from category where Category_ID='$category_id'");
                $row0=mysqli_fetch_assoc($result0)
            ?>
        <!-- subcategory -->
        <h3><i class="fa fa-angle-right"></i> <?php echo $row0['Category_name'] ?> - Subcategories</h3>
        <a href="main_category.php" style="color:white"><button type="button" class="btn btn-round btn-danger"><i class="fa fa-arrow-left"></i> BACK</button></a>
        <div class="row">
          <div class="col-md-12 mt">
            <div class="content-panel">
            <a href="add_subcategory.php?id=<?php echo $category_id ?>" style="color:white"><button type="button" class="btn btn-round btn-success pull-right"><i class="fa fa-plus"></i> Add</button></a>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sub-Categories</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                      $result = mysqli_query($db, "SELECT * from subcategory where cate_ID='$category_id'");
                      //$count = mysqli_num_rows($result2);//used to count number of rows
                      $num=0;
                    while($row=mysqli_fetch_assoc($result))
                    {
                      $num++;
                    ?>
                  <tr>
                    <td><?php echo $num; ?></td>
                    <td><?php echo $row['Subcategory_name']; ?></td>
                    <td>
                      <a href="edit_subcategory.php?id=<?php echo $row['Subcategory_ID']?>&cate_id=<?php echo $category_id ?>" ><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Edit</button></a>
                      <a href="subcategory.php?delete_sub_id=<?php echo $row['Subcategory_ID'];?>&cate_id=<?php echo $category_id; ?>" onclick="return confirmation()">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i> Delete</button>
                      </a>
                    </td>
                  </tr>
                  <?php
                  } 
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /col-md-12 -->
        </div>
        
        <?php 
         }
        ?>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

  <script type="text/javascript">

  //create a javascript function named confirmation()
  function confirmation()
  {
    var answer;
    answer=confirm("Do you want to delete this movie?");
    return answer;
  }
  </script>
                
</body>

</html>
<?php

//delete subcategory
if (isset($_GET["delete_sub_id"])) 
{
  $delete_subcate_id=$_GET["delete_sub_id"];
  $cate_id=$_GET["cate_id"];
	
  mysqli_query($db,"DELETE from subcategory where Subcategory_ID='$delete_subcate_id'");

  header( "refresh:0; url=subcategory.php?id=$cate_id" );
  ob_end_flush();

}


?>