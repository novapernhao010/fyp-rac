<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php");
      ob_start();
?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
      <?php
        if(isset($_GET["id"]))
        {
          $subcate_id=$_GET["id"];
          $cate_id=$_GET["cate_id"];

          $result = mysqli_query($db, "SELECT * from subcategory where Subcategory_ID='$subcate_id'");
          $row = mysqli_fetch_assoc($result);
          
      ?>
        <h3><i class="fa fa-angle-right"></i> Edit Subcategory</h3>
        <div class="row">
          <div class="col-md-12 mt">
            <div class="content-panel">
              <form role="form" class="form-horizontal" method="post">
                <div class="form-group">
                  <label class="col-lg-2 control-label">Subcategory Name</label>
                  <div class="col-lg-6">
                    <input type="text" placeholder=" " class="form-control" name="edit_subcategory" value="<?php echo $row['Subcategory_name']; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-theme" type="submit" name="savebtn">Save</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /col-md-12 -->
        </div>
        
        <?php 
         }
        ?>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
                
</body>

</html>
<?php
//edit subcategory
if(isset($_POST["savebtn"]))	
{
  $subcate_name=$_POST["edit_subcategory"];
	
    mysqli_query($db,"UPDATE subcategory SET Subcategory_name='$subcate_name' where Subcategory_ID='$subcate_id'");
	
	header( "refresh:0; url=subcategory.php?id=$cate_id" );
  ob_end_flush();
}


?>