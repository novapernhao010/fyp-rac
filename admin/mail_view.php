<!DOCTYPE html>
<html lang="en">
  <?php include("html_head.php") ?>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashio - Bootstrap Admin Template</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <?php include("header.php") ?>
      
    <?php include("sidebar.php") ?>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
                <section class="panel">
                  <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked labels-info ">
                      <li>
                        <h4>Admin Online</h4>
                      </li>
                      <li>
                        <a href="#">
                            <img src="img/friends/fr-05.jpg" class="img-circle" width="20">Pern Juin Hao
                            <p><span class="label label-success">Available</span></p>
                          </a>
                      </li>
                      <li>
                        <a href="#">
                            <img src="img/friends/fr-05.jpg" class="img-circle" width="20">Darren Soo
                            <p><span class="label label-danger"> Busy</span></p>
                          </a>
                      </li>
                      <li>
                        <a href="#">
                            <img src="img/friends/fr-01.jpg" class="img-circle" width="20">Vincent Toh
                            <p><span class="label label-danger"> Busy</span>Busy</p>
                          </a>
                      </li>
                      <li>
                        <a href="#">
                            <img src="img/friends/fr-03.jpg" class="img-circle" width="20">Phillip
                            <p>Offline</p>
                          </a>
                      </li>
                      <li>
                        <a href="#">
                            <img src="img/friends/fr-02.jpg" class="img-circle" width="20">Joshua
                            <p>Offline</p>
                          </a>
                      </li>
                    </ul>
                    <a href="#"> + Add More</a>
                    <div class="inbox-body text-center inbox-action">
                      <div class="btn-group">
                        <a class="btn mini btn-default" href="javascript:;">
                          <i class="fa fa-power-off"></i>
                          </a>
                      </div>
                      <div class="btn-group">
                        <a class="btn mini btn-default" href="javascript:;">
                          <i class="fa fa-cog"></i>
                          </a>
                      </div>
                    </div>
                  </div>
                </section>
            </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    View Message
                    
                  </h4>
              </header>
              <div class="panel-body ">
                <div class="mail-header row">
                  <div class="col-md-8">
                    <h4 class="pull-left"> Dashio, New Admin Dashboard & Front-end </h4>
                  </div>
                  <div class="col-md-4">
                    <div class="compose-btn pull-right">
                      <a href="mail_compose.php" class="btn btn-sm btn-theme"><i class="fa fa-reply"></i> Reply</a>
                      <button class="btn btn-sm tooltips" data-original-title="Trash" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-trash-o"></i></button>
                    </div>
                  </div>
                </div>
                <div class="mail-sender">
                  <div class="row">
                    <div class="col-md-8">
                      <img src="img/ui-zac.jpg" alt="">
                      <strong>Zac Doe</strong>
                      <span>[zac@youremail.com]</span> to
                      <strong>me</strong>
                    </div>
                    <div class="col-md-4">
                      <p class="date"> 10:15AM 02 FEB 2014</p>
                    </div>
                  </div>
                </div>
                <div class="view-mail">
                  <p>As he bent his head in his most courtly manner, there was a secrecy in his smiling face, and he conveyed an air of mystery to those words, which struck the eyes and ears of his nephew forcibly. At the same time, the thin straight lines
                    of the setting of the eyes, and the thin straight lips, and the markings in the nose, curved with a sarcasm that looked handsomely diabolic. </p>
                  <p>"Yes," repeated the Marquis. "A Doctor with a daughter. Yes. So commences the new philosophy! You are fatigued. Good night!"</p>
                  <p>It would have been of as much avail to interrogate any stone face outside the chateau as to interrogate that face of his. The nephew looked at him, in vain, in passing on to the door. </p>
                  <p>"Good night!" said the uncle. "I look to the pleasure of seeing you again in the morning. Good repose! Light Monsieur my nephew to his chamber there!--And burn Monsieur my nephew in his bed, if you will," he added to himself, before
                    he rang his little bell again, and summoned his valet to his own bedroom.</p>
                </div>
                <br>
                <div class="compose-btn pull-left">
                  <a href="mail_compose.php" class="btn btn-sm btn-theme"><i class="fa fa-reply"></i> Reply</a>
                  <button class="btn btn-sm tooltips" data-original-title="Trash" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-trash-o"></i></button>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="mail_view.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>

</html>
