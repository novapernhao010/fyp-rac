<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php") ?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Transaction History</h3>
        <div class="row mt">
          <div class="col-lg-12">
                    <!-- row -->
        <div class="row mt">
          <div class="col-md-12">
            <div class="content-panel">
              <table class="table table-striped table-advance table-hover">
                <h4><i class="fa fa-angle-right"></i>Customer Transaction History</h4>
                <hr>
                <thead>
                  <tr>
                    <th><i class="fa fa-user"></i> Name</th>
                    <th class="hidden-phone"><i class="fa fa-shopping-cart"></i> Items</th>
                    <th><i class="fa fa-usd"></i> Price</th>
                    <th><i class=" fa fa-clock-o"></i> Status</th>
                    <th><i class=" fa fa-pencil"></i> Editor</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <a href="basic_table.html#">Sliver</a>
                    </td>
                    <td class="hidden-phone">Red Lens Full LED Bumper Reflector Lights For 2018-up Honda Accord Sedan, Function as Tail/Brake, Turn Signal Lights & Rear Fog Lamps</td>
                    <td>RM239.00 </td>
                    <td><span class="label label-info label-mini">Pending</span></td>
                    <td>
                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href="basic_table.html#">
                        Vincc
                        </a>
                    </td>
                    <td class="hidden-phone">Infinity Car Speakers – Kappa & Reference Series</td>
                    <td>RM258.00 </td>
                    <td><span class="label label-warning label-mini">Paid</span></td>
                    <td>
                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href="basic_table.html#">
                        JJ
                        </a>
                    </td>
                    <td class="hidden-phone">Branded Car Polish Machine</td>
                    <td>RM474.00 </td>
                    <td><span class="label label-success label-mini">Pending</span></td>
                    <td>
                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href="basic_table.html#">TZ</a>
                    </td>
                    <td class="hidden-phone">Branded Car Polish Machine</td>
                    <td>RM474.00 </td>
                    <td><span class="label label-success label-mini">Paid</span></td>
                    <td>
                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href="basic_table.html#">Ming</a>
                    </td>
                    <td class="hidden-phone">Red Lens Full LED Bumper Reflector Lights For 2018-up Honda Accord Sedan, Function as Tail/Brake, Turn Signal Lights & Rear Fog Lamps</td>
                    <td>RM239.00 </td>
                    <td><span class="label label-warning label-mini">Pending</span></td>
                    <td>
                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-md-12 -->
        </div>
        <!-- /row -->
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>

</html>