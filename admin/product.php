<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php");
      ob_start();
?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Products List</h3>
        <div class="row">
          <div class="col-md-12 mt">
            <div class="content-panel">
            <a href="add_product.php" style="color:white"><button type="button" class="btn btn-round btn-success pull-right"><i class="fa fa-plus"></i> Add</button></a>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th></th>
                    <th>Product Name</th>
                    <th>Product Details</th>
                    <!-- <th>Subcategory</th> -->
                    <th>Category</th>
                    <th>Subcategory</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
			
                    $result = mysqli_query($db, "SELECT * from product");	
                    $count = mysqli_num_rows($result);//used to count number of rows
                    $num=0;
                    while($row = mysqli_fetch_assoc($result))
                    {
                      $num++;
                    ?>
                  
                  <tr>
                    <?php 
                        $subcate_result=mysqli_query($db,"SELECT Subcategory_name from subcategory where Subcategory_ID='".$row['Prod_subcate_ID']."' ");
                        $subcate_name=mysqli_fetch_assoc($subcate_result);
                        $cate_result=mysqli_query($db,"SELECT Category_name from category where Category_ID='".$row['Prod_cate_ID']."' ");
                        $cate_name=mysqli_fetch_assoc($cate_result);
                    ?>

                    <td><?php echo $num; ?></td>
                    <td></td>
                    <td><?php echo $row['Prod_name']; ?></td>
                    <td><?php echo $row['Prod_details']; ?></td>
                    <td><?php echo $cate_name['Category_name']; ?></td>
                    <td><?php echo $subcate_name['Subcategory_name']; ?></td>
                    <td><?php echo $row['Prod_price']; ?></td>
                    <td><?php echo $row['Prod_stock']; ?></td>
                    <td>
                      <a href="edit_product.php?id=<?php echo $row['Prod_ID'];?>"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>  View & Edit</button></a>
                      <a href="product.php?id=<?php echo $row['Prod_ID'];?>" onclick="return confirmation()">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i> Delete</button>
                      </a>
                      <a href="" style="color:green"><button class="btn btn-xs"><i class="fa fa-circle"></i> ON Selling</button></a> 
                    </td>
                  </tr>
                  <?php
                  } 
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /col-md-12 -->
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

  <script type="text/javascript">

  //create a javascript function named confirmation()
  function confirmation()
  {
    var answer;
    answer=confirm("Do you want to delete this product?");
    return answer;
  }
  </script>
                
</body>

</html>
<?php
//delete product
if (isset($_GET["id"])) 
{
	$prod_id=$_GET["id"];
  $delete_img=mysqli_query($db,"SELECT Prod_img1,Prod_img2,Prod_img3,Prod_img4,Prod_img5 from product where Prod_ID='$prod_id'");
  $delete=mysqli_fetch_assoc($delete_img);
  unlink('../rac/product_img/'.$delete['Prod_img1']);
  unlink('../rac/product_img/'.$delete['Prod_img2']);
  unlink('../rac/product_img/'.$delete['Prod_img3']);
  unlink('../rac/product_img/'.$delete['Prod_img4']);
  unlink('../rac/product_img/'.$delete['Prod_img5']);
  
	mysqli_query($db,"DELETE from product where Prod_ID='$prod_id'");
  header("refresh:0; url=product.php"); //refresh the page
  ob_end_flush();
}

?>