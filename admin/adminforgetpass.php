
<!DOCTYPE html>
<html lang="zxx">
<?php include("html_head.php"); ?>
<style>
input[type=email]{
    width:90%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: none;
    border-bottom: 2px solid red;
}
input[type=submit]{
    height: 1.3cm;
    background-color: #f44336;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    border: none;
    color: white;
}
</style>
<body>
    <div class="main-agile">
    <div class="contact py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>Recover Password</span>
			</h3>
					<div class="signin-form recover-password">
						<form action="" name="forgetform" method="post">
							<input type="email" style="font-style:italic;" placeholder="Please Enter Your Email (Admin)" name="email" size="50" required />
							<input type="submit" class="send" name="pass" value="Request">
                            <br>
                            <br>
							<div class="signin-agileits-bottom"> 
								<p><a href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span style="font-size:25px;"> Remember Password ? Go back to Index Page to Login</span></a></p>    
							</div>
						</form>
					</div>
			</div>
           </div>
         </div>
	 <?php

function random_pin()
	{
		$character_set_array = array();
		$character_set_array[] = array('count' => 5, 'characters' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$character_set_array[] = array('count' => 3, 'characters' => '0123456789');
		$temp_array = array();
		foreach ($character_set_array as $character_set) 
		{
			for ($i = 0; $i < $character_set['count']; $i++) 
			{
				$temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
			}
	}
		shuffle($temp_array);
		return implode('', $temp_array);
	}
	
	if(isset($_POST["pass"]))
	{
		$email =($_POST['email']);
		// $currdate = date("Y-m-d H:i:s");  	
		// $after_date = date("Y-m-d H:i:s",strtotime("+1 hours", strtotime($currdate)));
		$email_check = mysqli_query($db,"select * from admin where Admin_email = '$email'");
		$count = mysqli_num_rows($email_check);
		
		if($count != 0)
		{
			$pin = random_pin();

            $result = mysqli_query($db,"update admin set Admin_pincode ='$pin' where Admin_email = '$email'");
			$subject = "Forgot Password Security Pin (Admin)";
			$message = "You can change your password by using this security pin and click the link at below:\n\n http://localhost/fyp-rac/admin/adminresetpass.php \n\nSecurity Pin:".$pin." \n\n Thank you.\n\nThe sucurity pin is just only valid within 1 hour.";
			$from = 'From: RAC Car Accessories <raccar0825@gmail.com>' . "\r\n";
			
			mail($email,$subject,$message,$from);	
			
		
	?>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>		
	<script type="text/javascript">
		   swal({
			   title: "Successful!",
			   text:"You can check your email to reset password.",
			   icon:"success"
			   }).then(function(){window.location.href="adminresetpass.php";});
	</script>
	<?php
		}
		  else
		  {
			  ?>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>		
	<script type="text/javascript">
		   swal({
			   title: "OPPS Email Incorrect!",
			   text:"Please key in correct E-mail!",
			   icon:"error"
			   }).then(function(){window.location.href="adminforgetpass.php";});
	</script>
	<?php
		  }
	}
?>
</body>
</html>