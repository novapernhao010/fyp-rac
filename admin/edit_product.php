<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php");
      ob_start();
?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Edit Product</h3>
        <a href="product.php" style="color:white"><button type="button" class="btn btn-round btn-danger"><i class="fa fa-arrow-left"></i> BACK</button></a>
        <div class="row">
          <div class="col-lg-12">
            <div class="form-panel">
                <?php
                    if(isset($_GET["id"]));
                    {
                        $prod_id=$_GET["id"];
                        $result=mysqli_query($db,"SELECT * from product where Prod_ID='$prod_id'");
                        $prod=mysqli_fetch_assoc($result);
                ?>
              <form class="form-horizontal style-form" method="post" enctype="multipart/form-data" action="">
                <div class="form-group">
                  <label class="col-lg-2 control-label">Product Name</label>
                  <div class="col-lg-6">
                    <input type="text" placeholder=" " class="form-control" name="prod_name" required value="<?php echo $prod['Prod_name'] ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Product Description</label>
                  <div class="col-lg-6">
                    <textarea class="form-control" name="prod_details" cols="100" rows="6"><?php echo $prod['Prod_details'] ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Price (RM)</label>
                  <div class="col-lg-3">
                    <input type="number" value="<?php echo $prod['Prod_price'] ?>" class="form-control" name="prod_price" min="0.00" max="9999999.99" step="0.01" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Stock</label>
                  <div class="col-lg-2">
                    <input type="number" class="form-control" name="prod_stock" min="0" max="999999" value="<?php echo $prod['Prod_stock'] ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Category</label>
                  <div class="col-lg-6">
                    <select class="form-control" name="prod_category" id="category">
                        <option value=""></option>
                        <?php 
                            $result=mysqli_query($db,"SELECT * FROM category");
                            while($row=mysqli_fetch_assoc($result))
                            {
                              if($row['Category_ID']==$prod['Prod_cate_ID'])
                              {
                               ?>
                                <option value="<?php echo $row['Category_ID'] ?>" selected><?php echo $row['Category_name'] ?></option>
                              <?php
                                continue;
                              }
                              else
                              {
                                ?>
                                <option value="<?php echo $row['Category_ID'] ?>"><?php echo $row['Category_name'] ?></option>
                                <?php
                                continue;
                              }
                            }
                        ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Subcategory</label>
                    <div class="col-lg-6">
                    <?php
                      $result=mysqli_query($db,"SELECT * from subcategory where Subcategory_ID='".$prod['Prod_subcate_ID']."'");
                      $row=mysqli_fetch_assoc($result);
                    ?>
                        <select class="form-control" name="prod_subcategory" id="subcategory">
                          <option value="<?php echo $row['Subcategory_ID'] ?>" selected><?php echo $row['Subcategory_name'] ?></option>
                        </select>
                    </div>
                </div>
                <!-- image input -->
                <div class="form-group">
                  <label class="control-label col-md-3">Image 1</label>
                  <div class="col-md-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                    <?php
                      $path="../rac/product_img/";
                    ?>
                      <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo $path.$prod['Prod_img1'] ?>" alt="" />
                      </div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                      <div>
                        <span class="btn btn-theme02 btn-file">
                          <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                        <input type="file" class="default" name="prod_img1" accept="image/*"/>
                        </span>
                        <!-- <a href="#" class="btn btn-theme04 fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Image 2</label>
                  <div class="col-md-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo $path.$prod['Prod_img2'] ?>" alt="" />
                      </div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                      <div>
                        <span class="btn btn-theme02 btn-file">
                          <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                        <input type="file" class="default" name="prod_img2"/>
                        </span>
                        <!-- <a href="add_product.php#" class="btn btn-theme04 fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Image 3</label>
                  <div class="col-md-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo $path.$prod['Prod_img3'] ?>" alt="" />
                      </div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                      <div>
                        <span class="btn btn-theme02 btn-file">
                          <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                        <input type="file" class="default" name="prod_img3"/>
                        </span>
                        <!-- <a href="add_product.php#" class="btn btn-theme04 fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Image 4</label>
                  <div class="col-md-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo $path.$prod['Prod_img4'] ?>" alt="" />
                      </div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                      <div>
                        <span class="btn btn-theme02 btn-file">
                          <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                        <input type="file" class="default" name="prod_img4"/>
                        </span>
                        <!-- <a href="add_product.php#" class="btn btn-theme04 fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Image 5</label>
                  <div class="col-md-9">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo $path.$prod['Prod_img5'] ?>" alt="" />
                      </div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                      <div>
                        <span class="btn btn-theme02 btn-file">
                          <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                        <input type="file" class="default" name="prod_img5"/>
                        </span>
                        <!-- <a href="add_product.php#" class="btn btn-theme04 fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <!-- //image input -->
                <div class="form-group">
                  <label class="col-lg-2 control-label">Selling status</label>
                  <div class="col-lg-2">
                    <div class="radio">
                        <label>
                            <input type="radio" name="prod_status" id="1" value="1" checked>On
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="prod_status" id="0" value="0" >Off
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-theme" type="submit" name="savebtn">Save</button>
                  </div>
                </div>
              </form>
            <?php } ?>
            </div>
          </div>
          <!-- /col-md-12 -->
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script src="lib/advanced-form-components.js"></script>

  <!-- get subcate -->
  <script>
$(document).ready(function() {
	$('#category').on('change', function() {
			var category_id = this.value;
			$.ajax({
				url: "get_subcate.php",
				type: "POST",
				data: {
					category_id: category_id
				},
				cache: false,
				success: function(dataResult){
					$("#subcategory").html(dataResult);
				}
			});
		
		
	});
});
</script>

</body>

</html>
<?php
//edit product
if(isset($_POST["savebtn"])) 	
{
  $target_dir = "../rac/product_img/";
  $extensions_arr = array("jpg","jpeg","png","gif");
  $delete_img=mysqli_query($db,"SELECT Prod_img1,Prod_img2,Prod_img3,Prod_img4,Prod_img5 from product where Prod_ID='".$prod['Prod_ID']."'");
  $delete=mysqli_fetch_assoc($delete_img);
  if(count($_FILES) > 0) {
    if($_FILES['prod_img1']['name']!='') {
      //Delete previous img
      unlink($target_dir.$delete['Prod_img1']);
      // Get file type
      $imageFileType = strtolower(pathinfo($_FILES['prod_img1']['name'],PATHINFO_EXTENSION));
      $prod_img1 = $prod['Prod_ID']."-(1).".$imageFileType;
      $target_file = $target_dir.$prod_img1;
      // Valid file extensions
      if( in_array($imageFileType,$extensions_arr) )
      {
        move_uploaded_file($_FILES['prod_img1']['tmp_name'],$target_file);
        mysqli_query($db,"UPDATE product SET Prod_img1='$prod_img1' where product.Prod_ID='".$prod['Prod_ID']."'");
      }
    }

    if($_FILES['prod_img2']['name']!='') {
      //Delete previous img
      unlink($target_dir.$delete['Prod_img2']);
      // Get file type
      $imageFileType = strtolower(pathinfo($_FILES['prod_img2']['name'],PATHINFO_EXTENSION));
      $prod_img2 = $prod['Prod_ID']."-(2).".$imageFileType;
      $target_file = $target_dir.$prod_img2;
      // Valid file extensions
      if( in_array($imageFileType,$extensions_arr) )
      {
        move_uploaded_file($_FILES['prod_img2']['tmp_name'],$target_file);
        mysqli_query($db,"UPDATE product SET Prod_img2='$prod_img2' where product.Prod_ID='".$prod['Prod_ID']."'");
      }
    }

    if($_FILES['prod_img3']['name']!='') {
      //Delete previous img
      unlink($target_dir.$delete['Prod_img3']);
      // Get file type
      $imageFileType = strtolower(pathinfo($_FILES['prod_img3']['name'],PATHINFO_EXTENSION));
      $prod_img3 = $prod['Prod_ID']."-(3).".$imageFileType;
      $target_file = $target_dir.$prod_img3;
      // Valid file extensions
      if( in_array($imageFileType,$extensions_arr) )
      {
        move_uploaded_file($_FILES['prod_img3']['tmp_name'],$target_file);
        mysqli_query($db,"UPDATE product SET Prod_img3='$prod_img3' where product.Prod_ID='".$prod['Prod_ID']."'");
      }
    }

    if($_FILES['prod_img4']['name']!='') {
      //Delete previous img
      unlink($target_dir.$delete['Prod_img4']);
      // Get file type
      $imageFileType = strtolower(pathinfo($_FILES['prod_img4']['name'],PATHINFO_EXTENSION));
      $prod_img4 = $prod['Prod_ID']."-(4).".$imageFileType;
      $target_file = $target_dir.$prod_img4;
      // Valid file extensions
      if( in_array($imageFileType,$extensions_arr) )
      {
        move_uploaded_file($_FILES['prod_img4']['tmp_name'],$target_file);
        mysqli_query($db,"UPDATE product SET Prod_img4='$prod_img4' where product.Prod_ID='".$prod['Prod_ID']."'");
      }
    }

    if($_FILES['prod_img5']['name']!='') {
      //Delete previous img
      unlink($target_dir.$delete['Prod_img5']);
      // Get file type
      $imageFileType = strtolower(pathinfo($_FILES['prod_img5']['name'],PATHINFO_EXTENSION));
      $prod_img5 = $prod['Prod_ID']."-(5).".$imageFileType;
      $target_file = $target_dir.$prod_img5;
      // Valid file extensions
      if( in_array($imageFileType,$extensions_arr) )
      {
        move_uploaded_file($_FILES['prod_img5']['tmp_name'],$target_file);
        mysqli_query($db,"UPDATE product SET Prod_img5='$prod_img5' where product.Prod_ID='".$prod['Prod_ID']."'");
      }
    }
  }
  $prod_name = $_POST["prod_name"];
  $prod_details = nl2br($_POST["prod_details"]);
  $prod_price = $_POST["prod_price"];
  $prod_stock = $_POST["prod_stock"];
  $prod_status = $_POST["prod_status"];
  $prod_cate = $_POST["prod_category"];
  $prod_subcate = $_POST["prod_subcategory"]; 	
  
  mysqli_query($db,"UPDATE product SET Prod_name='$prod_name',Prod_details='$prod_details',Prod_price='$prod_price',Prod_stock='$prod_stock',
  Prod_status='$prod_status',Prod_cate_ID='$prod_cate',Prod_subcate_ID='$prod_subcate'
  where product.Prod_ID='".$prod['Prod_ID']."'"); 
  
  
  header("refresh:0; url=product.php");
  ob_end_flush();

}

?>