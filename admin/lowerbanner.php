<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php") ?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Banner Editor</h3>
        <div class="row mt">
          <div class="col-lg-12">
            <p>Please edit banner here.</p>
            <div class="panel panel-default">
                        <div class="panel-heading">
                            Banner Editor
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <h4>Lower Banner</h4>
                            <button type="button" class="btn btn-primary btn-circle" onclick="document.location='edit2.php'"><i class="fa fa-edit"> Edit</i>
                            </button>
                            <button type="button" class="btn btn-warning btn-circle" onclick="document.location='lowwerlist.php'"><i class="fa fa-list"> List</i>
                            </button>
                            <button type="button" class="btn btn-danger btn-circle"><i class="fa fa-trash"> Delete</i>
                            </button>
                            <button type="button" class="btn btn-success btn-circle"><i class="fa fa-save"> Save</i>
                            </button>
                        <!-- /.panel-body -->
                    </div>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>

</html>