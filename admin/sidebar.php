    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.php"><img src="img/user.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered"><span style="color:#99ccff;font-size:150%;"><?php echo $adminData['Admin_name']; ?><span></h5>
          <li class="mt">
            <a class="active" href="index.php">
              <i class="fa fa-home"></i>
              <span>Home</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-tags"></i>
              <span>Product</span>
              <span class="fa fa-chevron-down pull-right"></span>
              </a>
            <ul class="sub">
              <li><a href="main_category.php">Category</a></li>
              <li><a href="product.php">Product List</a></li>
              <li><a href="add_product.php">Add Product</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-desktop"></i>
              <span>Advertisement</span>
              <span class="fa fa-chevron-down pull-right"></span>
              </a>
            <ul class="sub">
            <li><a href="topbanner.php">Top Banner</a></li>
              <li><a href="middlebanner.php">Middle Banner</a></li>
              <li><a href="lowerbanner.php">Lower Banner</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="history.php">
              <i class="fa fa-clock-o"></i>
              <span>Transaction History</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-users"></i>
              <span>Customer</span>
              <span class="fa fa-chevron-down pull-right"></span>
              </a>
            <ul class="sub">
              <li><a href="customer.php">Manage Customers</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-suitcase"></i>
              <span>Admin</span>
              <span class="fa fa-chevron-down pull-right"></span>
              </a>
            <ul class="sub">
              <li><a href="manage_admin.php">Manage Admin</a></li>
            </ul>
          </li>
          <li>
            <a href="inbox.php">
              <i class="fa fa-envelope"></i>
              <span>Messages </span>
              <span class="label label-theme pull-right mail-info">2</span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
  