<!DOCTYPE html>
<html lang="en">

<?php include("html_head.php");
      ob_start();
?>

<body>
  <section id="container">
    <?php include("header.php") ?>
    <?php include("sidebar.php") ?> 
    <!-- **
        MAIN CONTENT
        *** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Categories</h3>
        <div class="row">
          <div class="col-md-12 mt">
            <div class="content-panel">
            <a href="#" data-toggle="modal" data-target="#exampleModal" style="color:white"><button type="button" class="btn btn-round btn-success pull-right"><i class="fa fa-plus"></i> Add</button></a>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Main Categories</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
			
                    $result = mysqli_query($db, "SELECT * from category");	
                    $count = mysqli_num_rows($result);//used to count number of rows
                    $num=0;
                    while($row = mysqli_fetch_assoc($result))
                    {
                      $num++;
                    ?>
                  
                  <tr>
                    
                    <td><?php echo $num; ?></td>
                    <td><?php echo $row['Category_name']; ?></td>
                    <td>
                      <a href="subcategory.php?id=<?php echo $row['Category_ID']; ?>"><button class="btn btn-success btn-xs"><i class="fa fa-list"></i> View Subcategory</button></a>
                      <a href="edit_main_category.php?id=<?php echo $row['Category_ID'];?>"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Edit</button></a>
                      <a href="main_category.php?id=<?php echo $row['Category_ID'];?>" onclick="return confirmation()">
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i> Delete</button>
                      </a>
                    </td>
                  </tr>
                  <?php
                  } 
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /col-md-12 -->
        </div>
        <!-- Add category -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title text-center">Add New Main Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="#" method="post">
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Category Name</label>
                    <div class="col-sm-10">
                      <input type="text" value="" class="form-control" placeholder=" " name="category">
                    </div>
                  </div>
                  <button class="btn btn-theme form-control" type="submit" name="addbtn">Add Category</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /add category -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php") ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

  <script type="text/javascript">

  //create a javascript function named confirmation()
  function confirmation()
  {
    var answer;
    answer=confirm("Do you want to delete this category?");
    return answer;
  }
  </script>
                
</body>

</html>
<?php
if(isset($_POST["addbtn"])) 	
{
	$cate = $_POST["category"];  	
	
	mysqli_query($db,"INSERT INTO category(Category_name) VALUES('$cate')");
  
  
  header("refresh:0; url=main_category.php");
  ob_end_flush();
}

if (isset($_GET["id"])) 
{
	$cate_id=$_GET["id"];
	
  mysqli_query($db,"DELETE from subcategory where cate_ID='$cate_id'");
  mysqli_query($db,"DELETE from category where Category_ID='$cate_id'");
  header("refresh:0; url=main_category.php"); //refresh the page
  ob_end_flush();
}

?>